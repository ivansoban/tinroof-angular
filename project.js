angular.module('project', ['ngRoute'])

.service('JSONFetch', function($http) {
    var self = this;
    this.fetch = function(url) {
        return $http.get(url);
    };
})

.config(function($routeProvider) {
    var resolveUsers = {
        users: function(JSONFetch) {
            return JSONFetch.fetch("http://jsonplaceholder.typicode.com/users");
        }
    };

    var resolveAlbums = {
        albums: function(JSONFetch) {
            return JSONFetch.fetch("http://jsonplaceholder.typicode.com/albums");
        },
        users: function(JSONFetch) {
            return JSONFetch.fetch("http://jsonplaceholder.typicode.com/users");
        }
    };

    var resolvePhotos = {
        photos: function(JSONFetch) {
            return JSONFetch.fetch("http://jsonplaceholder.typicode.com/photos");
        },
        albums: function(JSONFetch) {
            return JSONFetch.fetch("http://jsonplaceholder.typicode.com/albums");
        }
    };

    $routeProvider.when('/', {
        controller:'UserListController as userList',
        templateUrl:'users.html',
        resolve: resolveUsers
    })
    .when('/user/:userID', {
        controller:'UserController as albumList',
        templateUrl:'user.html',
        resolve: resolveAlbums
    })
    .when('/album/:albumID', {
        controller:'AlbumController as photoList',
        templateUrl:'album.html',
        resolve: resolvePhotos
    })
    .otherwise({
        redirectTo:'/'
    });
})

.controller('UserListController', function(users) {
    var userList = this;
    if (users.status == 200) {
        userList.users = users.data;
    } else {
        userList.error = users.data;
    }
})

.controller('UserController', function($routeParams, $scope, albums, users) {
    var albumList = this;
    albumList.userID = $routeParams.userID;

    if (albums.status == 200) {
        albumList.albums = albums.data.filter(function(album) {
            return album.userId == $routeParams.userID;
        });
    } else {
        albumList.error = albums.data;
        return;
    }

    if (users.status == 200) {
        albumList.user = users.data.filter(function(user) {
            return user.id == $routeParams.userID;
        })[0];
    } else {
        albumList.error = users.data;
        return;
    }

    $scope.currentPage   = 0;
    $scope.pageSize      = 5;
    $scope.length        = albumList.albums.length;
    $scope.numberOfPages = function() {
        return Math.ceil($scope.length/$scope.pageSize);
    };
})

.controller('AlbumController', function($routeParams, $scope, photos, albums) {
    var photoList = this;

    if (albums.status == 200) {
        photoList.album = albums.data.filter(function(album) {
            return album.id == $routeParams.albumID;
        })[0];
    } else {
        photoList.error = albums.data;
        return;
    }

    if (photos.status == 200) {
        photoList.photos = photos.data.filter(function(photo) {
            return photo.albumId == $routeParams.albumID;
        });
    } else {
        photoList.error = photos.data;
        return;
    }

    $scope.currentImage  = null;
    $scope.currentPage   = 0;
    $scope.pageSize      = 10;
    $scope.length        = photoList.photos.length;
    $scope.numberOfPages = function() {
        return Math.ceil($scope.length/$scope.pageSize);
    };
})

.filter('startFrom', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    }
});
